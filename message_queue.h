#ifndef message_queue_h
#define message_queue_h

typedef struct banker_queue_t {
    int num_msgs,
    int key,
    int *msgs
};

typedef struct reply_queue_t {
    int num_msgs,
    int key,
    int *msgs
};

#endif