#ifndef banker_h
#define banker_h

#include "message.h"

typedef struct {
    int num_clients;
    int num_resource_types;
    int work[NUM_RESOURCE_TYPES];
    int safe[NUM_RESOURCE_TYPES];
    int req[NUM_RESOURCE_TYPES];
    int Allocated[NUM_CLIENTS + 1][NUM_RESOURCE_TYPES];
    int Max[NUM_CLIENTS + 1][NUM_RESOURCE_TYPES];
    int Need[NUM_CLIENTS + 1][NUM_RESOURCE_TYPES];
    int SysMax[NUM_RESOURCE_TYPES];
}banker_params_t;

static int limit_resource_types;
static int limit_num_clients;

#endif