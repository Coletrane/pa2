#ifndef message_h
#define message_h

#include <sys/types.h>

#define NUM_CLIENTS 10
#define NUM_RESOURCE_TYPES 5
#define MSG_INFO_SIZE (sizeof(msgBuf) + (NUM_CLIENTS * NUM_RESOURCE_TYPES) * sizeof(int))
#define BANKER 10000
#define CLIENT 9999

typedef enum {
    CLIENT_REQUESTING_RESOURCES = 1,
    CLIENT_RELEASING_RESOURCES = 2,
    CLIENT_STATING_FUTURE_MAX_CLAIMS = 3,
    BANKER_RESPONSE_REQUEST_GRANTED = 4,
    BANKER_RESPONSE_REQUEST_DENIED_SAFETY = 5,
    BANKER_RESPONSE_REQUEST_DENIED_MAX_CLAIMS_WOULD_BE_EXCEEDED = 6,
    BANKER_RESPONSE_RESOURCES_RELEASED = 7,
    BANKER_RESPONSE_RESOURCES_RELEASE_FAILED_ATTEMPTED_RELEASE_MORE_THAN_ALLOCATED = 8,
    BANKER_RESPONSE_INITIAL_REG_AND_MAX_CLAIMS_ACCEPTED = 9,
    BANKER_RESPONSE_INITIAL_REG_AND_MAX_CLAIMS_REJECTED = 10,
    CLIENT_RELEASE_ALL_RESOURCES = 11,
    BANKER_RESPONSE_REQUEST_DENIED_RESOURCES_UNAVAILABLE = 12,
}message_code;

typedef struct{
	long int serial_num;
	struct
	{
		pid_t banker_pid;
		pid_t client_pid;
		int client_id;
		int client_queue;
		int banker_queue;
		int banker_message_code;
		int client_message_code;
		int request_vector[NUM_RESOURCE_TYPES];
	}info;
}msgBuf;

#endif
