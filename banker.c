#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <semaphore.h>
#include <errno.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h> 

#include "banker.h"

FILE *in;
banker_params_t params;

void validate_message_request(msgBuf* rcv_msg);
void handle_message(msgBuf* rcv_msg);

int randr(int min,int max)
{
	double scaled = (double)rand()/RAND_MAX;
	
	return (max - min + 1) * scaled + min;
}

int main()
{
    
    // Variable declaration
    int queID;
    int msgStatus = 0;
    key_t queue_key;
	msgBuf rcv_msg, snd_msg;
	int safe , exec, counter;
	int remaingClients = NUM_CLIENTS;
 


    // Open the file 
    in = fopen("initial.data","r");
    active_clients = 0;
    // Initialize the parameters from the filea

    fscanf(in, "%d", &params.num_resource_types);
    //params.avail = malloc(NUM_RESOURCE_TYPES * sizeof(int));
   

    for (int i= 0; i < NUM_RESOURCE_TYPES; i++)
    {        
    	fscanf(in, "%d", &params.SysMax[i]);
    }
    /**
    params.Max = malloc(NUM_RESOURCE_TYPES * NUM_CLIENTS * sizeof(int));
    params.Allocated = malloc((NUM_RESOURCE_TYPES * NUM_CLIENTS) * sizeof(int));
    params.Need = malloc((NUM_RESOURCE_TYPES * NUM_CLIENTS) * sizeof(int));
    params.work = malloc(NUM_RESOURCE_TYPES * sizeof(int));
    params.running = malloc(NUM_RESOURCE_TYPES * sizeof(int));
    params.done = malloc(NUM_RESOURCE_TYPES * sizeof(int));
    params.req = malloc(NUM_RESOURCE_TYPES * sizeof(int));
    **/
    printf("Available System Resources: ");
    for (int i = 0;i < NUM_RESOURCE_TYPES;i++)
    {
        printf("%d  ",params.SysMax[i]);
    }
    printf("\n");
    // Initialize static upper limit of resource types
    limit_resource_types = NUM_RESOURCE_TYPES;
    
    // Create message queue
    queue_key = BANKER;
    queID = msgget(queue_key, IPC_CREAT | 0600);
    if (queID < 0)
    {
        printf("Failed to create mailbox %X. Error code=%d\n"
		,queID, errno);
        exit(-2);
    }
    printf("Banker QueueID: %d\n",queID);
    // Wait indefinitely for incoming messages
    while (1 > 0)
    {
    	//printf("Number of active clients: %d",active_clients);
    	msgStatus = msgrcv(queID, &rcv_msg, MSG_INFO_SIZE, 0, 0);
    	if (msgStatus < 0)
    	{
    		printf("Failed to receive message from Client process on queuID %d. Error code=%d\n"
    				,queID, errno);
            	exit(-2);
    	}
    	else
    	{
    		printf("Message received with Message Code: %d\n",rcv_msg.info.client_message_code);
    		switch (rcv_msg.info.client_message_code)
    		{
    			case 1 :
    				printf("Client is requesting resources\n");
    				for (int i = 0;i < NUM_RESOURCE_TYPES;i++)
    				{
    				     if (params.Max[i] <= params.Allocated[rcv_msg.info.client_id][i])
    				     {
    				    	 snd_msg.info.banker_message_code = 6;
    				     }
    				}
    				printf("The Request Vector from client %d is: ",(int)((rcv_msg.serial_num%1000)-1));
    				for (int i= 0; i < NUM_RESOURCE_TYPES; i++)
    				{
    					printf(" %d ", rcv_msg.info.request_vector[i]);
    					params.req[i] = rcv_msg.info.request_vector[i];
    				}
    				//Copy system resource to working resources
    				for (int i= 0; i < NUM_RESOURCE_TYPES; i++)
    				{
    					params.work[i] = params.SysMax[i];
    				}
    				for (int i= 1; i < NUM_CLIENTS + 1; i++)
    				{
    					for(int j = 0;j < NUM_RESOURCE_TYPES;j++)
    				    {
    						if (params.Max[i][j] > 0)
    							counter++;
    				    }
    					if (counter > 0)
    						params.safe[i] = 1;
    				}
    				printf("\n");
    				while (remaingClients != 0)
    				{
    					safe = 0;
    					for (int i = 1; i < NUM_CLIENTS+1; i++)
    					{
    						if (params.safe[i])
    						{
    							exec = 1;
    							for (int j = 1; j < NUM_RESOURCE_TYPES+1;j++)
    							{
    								if (params.Max[i][j] - params.Allocated[i][j] > params.work[j])
    								{
    									exec = 0;
    									break;
    								}
    								if (exec) 
    								{
    									params.safe[i] = 0;
    									remaingClients--;
    									safe = 1;
    									for (int j = 1;j < NUM_RESOURCE_TYPES+1;j++)
    									{
    										params.work[j] += params.Allocated[i][j];
    									}
    									break;
    								}
    							}
    						}
    						if (!safe)
    						{
    							snd_msg.serial_num = rcv_msg.serial_num;
    							snd_msg.info.banker_message_code = 5;
    							for (int j = 1;j < NUM_RESOURCE_TYPES+1;j++)
    							{
    							    params.work[j] -= params.Allocated[i][j];
    							}
    							break;
    						}
    						else
    						{
    							snd_msg.serial_num = rcv_msg.serial_num;
    							snd_msg.info.banker_message_code = 4;
    						}
    					}
    				}
    				printf("%d\n",snd_msg.info.banker_message_code);
    				if (snd_msg.info.banker_message_code == 4)
    				{
    					printf("The request is granted\n");
    				}
    				else
    					printf("The request is unsafe request declined\n");
    				break;

    			case 2 :
    				for (int j = 1;j < NUM_RESOURCE_TYPES+1;j++)
    				{
    					if (params.Allocated[snd_msg.info.client_id] < snd_msg.info.request_vector[j])
    						snd_msg.info.banker_message_code = 8;
    					else
    					{
    						printf("Releasing resources\n");
    						for (int j = 1;j < NUM_RESOURCE_TYPES+1;j++)
    						{
    							params.Allocated[snd_msg.info.client_id][j] -= snd_msg.info.request_vector[j];
    						}
    						snd_msg.info.banker_message_code = 7;
    					}
    				}
    				break;

    			case 3 :
    				printf("New Client Connected with serial num: %d and id: %d\n",
    						rcv_msg.serial_num, rcv_msg.info.client_id);
    				for (int j = 1; j < NUM_RESOURCE_TYPES+1;j++)
    				{
    					/**if (params.Max[rcv_msg.info.client_id][j] < )
    					{
    						
    					}**/
    				}
    				printf("Adding Max Resources...\n");
    				for (int i= 0; i < NUM_RESOURCE_TYPES; i++)
    				{
    					params.Max[rcv_msg.info.client_id][i] = rcv_msg.info.request_vector[i];
    					params.Need[rcv_msg.info.client_id][i] = (params.Max[rcv_msg.info.client_id][i] - params.Allocated[rcv_msg.info.client_id][i]);
    				}
    				printf("Max Table: \n");
    				for (int i= 1; i < NUM_CLIENTS + 1; i++)
    				{
    					for(int j = 0;j < NUM_RESOURCE_TYPES;j++)
    					{
    						printf(" %d ",params.Max[i][j]);
    					}
    					printf("\n");
    				}
    				printf("Adding Max Resources complete!\n");
    				printf("Need: \n");
    				for (int i = 1; i < NUM_CLIENTS + 1; i++)
    				{
    					for(int j = 0;j < NUM_RESOURCE_TYPES;j++)
    				    	{
    				    		printf(" %d ",params.Need[i][j]);
    				    	}
    				    printf("\n");
    				}
    				snd_msg.serial_num = rcv_msg.serial_num;
    				snd_msg.info.banker_message_code = 9;

    				break;
    		}
    		usleep(randr(100000, 500000));
    	}

    	//rcv_msg.info.client_pid = getpid();

    	msgStatus = msgsnd(queID, &snd_msg,  MSG_INFO_SIZE, 0);
    	if (msgStatus < 0)
    	{usleep(100000);
    		printf("Failed to send message to User process on queID %d. Error code=%d\n"
    				,queID, errno);
    		exit(-2);
    	}
    	else
    	{
    		printf("Successful transmission to client: %d\n",queID);
    	}
    }
  }
/**
void handle_message(msgBuf* rcv_msg)
{
	int i;
	int safe;
	msgBuf temp;	
	printf("message code: %d\n",rcv_msg->info.client_message_code);
	switch (rcv_msg->info.client_message_code)
	{
	case 1:
            printf("Client is requesting resources");
            validate_message_request(rcv_msg);
	    printf("The Request Vector from client %d is: ",(int)(rcv_msg->serial_num%1000));
	    for (i= 0; i < NUM_RESOURCE_TYPES; i++)
    	    {
		printf(" %d ", rcv_msg->info.request_vector[i]);
	    }
	    safe = 0;
	    for (i = 0; i < NUM_RESOURCE_TYPES;i++)
	    {
	    //	if (running[i
	    }
            break;
        case 2:
	    
        case 3:
	    printf("New Client Connected with id: %d",(int)(rcv_msg->serial_num%1000));
	    printf("Adding Max Resources of ("); 	
	    for (i= 0; i < NUM_RESOURCE_TYPES; i++)
    	    {
		params.Max[rcv_msg->info.client_id][i] = rcv_msg->info.request_vector[i];
		printf(" %d ",rcv_msg->info.request_vector[i]);
	    }
	    printf(")\n");
	    break;
	 case 12:
	    printf("Closing connection with client and releasing all resources");
           			        
	}

}**/
void validate_message_request(msgBuf* rcv_msg)
{
    int i;
    for (int i = 0;i < NUM_RESOURCE_TYPES;i++)
    {
        if (params.Max[i] <= params.Allocated[rcv_msg->info.client_id][i])
        {
            printf("Request Bad Exiting!");
        }
    }
}