all: client.h message.h banker.h
	gcc -w -std=c99 client.c -o client; gcc -w -std=c99 banker.c -o banker

client.o: client.h message.h
	gcc client.c -o client

banker.o: banker.h message.h
	gcc banker.c -o banker

clean:
	rm client; rm banker