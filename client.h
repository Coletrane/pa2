#ifndef client_h
#define client_h

#include "message.h"

typedef struct{
    int queue_key;
    int queue_id;
    int id;
    int num_resource_types;
    int allocated[NUM_RESOURCE_TYPES];
    int max_claim[NUM_RESOURCE_TYPES];
    int resource_max[NUM_RESOURCE_TYPES];
    int request_vector[NUM_RESOURCE_TYPES];
}client_t;


#endif
