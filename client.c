/*
*Client.c
*
*
*/

#ifndef client_c
#define client_c

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <semaphore.h>
#include <errno.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>

#include "client.h"

FILE *in;

int randr(int min,int max)
{
	double scaled = (double)rand()/RAND_MAX;
	
	return (max - min + 1) * scaled + min;
}
void sendMessage(msgBuf *msg, int queID)
{
    int msgStatus;
    msgStatus = msgsnd(queID, &msg, MSG_INFO_SIZE, 0);
    if (msgStatus < 0)
    {
        printf("Failed to send message to banker on id %d. Error code =%d\n"
            ,queID, errno);
        exit(-2);
    }
}

int main(int argc, char *argv[])
{
	msgBuf snd_msg, rcv_msg;
	key_t msgQueKey;
	int queID;
	int i;
	int serial_num;
	int req_bytes = 0;
	int rls_bytes = 0;
	int rcv_bool = 0;
	int init_bool = 1;

	client_t params;
	srand(time(NULL));
	active_clients++;
    // Open the file

    in = fopen("initial.data","r");
	fscanf(in, "%d", &params.num_resource_types);

	// Initialize resource type values
	for (i= 0; i < params.num_resource_types; i++)
    {
		fscanf(in, "%d", &params.resource_max[i]);
		params.max_claim[i] = randr(0,params.resource_max[i]);
		printf("Max claim: %d Resource types: %d\n", params.max_claim[i], params.resource_max[i]);
    }

	// Initialize client params
	params.id = *argv[1] - 48;
	serial_num = 1000 * params.id + 1;
	params.queue_key = CLIENT;
	params.queue_id = msgget(params.queue_key, IPC_CREAT | 0600);
	printf("client queid %d\n", params.queue_id);

	// Get banker's mailbox
	msgQueKey = BANKER;
	queID = msgget(msgQueKey, IPC_EXCL | 0600);
	if ( queID < 0)
	{
		printf("Failed to create mailbox %x. Error Code = %d\n"
			,msgQueKey,errno);
		exit(-2);
	}
	printf("Banker QueueID %d\n", queID);

	// Compose and send message of max demands
	message_code code = 3;
	snd_msg.info.client_message_code = 3;
	printf("message code = %d\n", snd_msg.info.client_message_code);
	
	snd_msg.serial_num = serial_num;
	printf("serial number = %d\n",snd_msg.serial_num);
	snd_msg.info.client_pid = getpid();
	snd_msg.info.client_id = params.id;
	snd_msg.info.client_queue = params.queue_id;
	for (int i = 0; i < NUM_CLIENTS; i++)
	{
		snd_msg.info.request_vector[i] = params.max_claim[i];
	}
	for (int i = 0; i < NUM_RESOURCE_TYPES; i++)
	{
		printf("%d\n", snd_msg.info.request_vector[i]);
	}

	int bytes = msgsnd(queID, &snd_msg, MSG_INFO_SIZE, 0);
	//int init_rec = msgrcv(queID, &rcv_msg, MSG_INFO_SIZE, snd_msg.serial_num, 0);


	// 10 Iterations
	for (int i = 0; i < 10; i++)
	{
		
		if (i != 0)
		{
			printf("number: %d\n",i);
			// Generate random request vector[]
			for (int j = 0; j < NUM_RESOURCE_TYPES; j++)
			{
				snd_msg.info.request_vector[i] = randr(0,params.max_claim[i]);
			}
			// Send message with request vector[]
			message_code code = 1;
			snd_msg.info.client_message_code= code;
			serial_num++;
			snd_msg.serial_num = serial_num;
			snd_msg.info.client_pid = getpid();
			snd_msg.info.client_id = params.id;
			snd_msg.info.client_queue = params.queue_id;
			//snd_msg.info.request_vector[NUM_RESOURCE_TYPES] = params.request_vector[NUM_RESOURCE_TYPES];
			bytes = msgsnd(queID, &snd_msg, MSG_INFO_SIZE, 0);
			//usleep(randr(100000, 5000000));
		}

		// Wait for response that contains this client's serial number
		int req_bytes = msgrcv(queID, &rcv_msg, MSG_INFO_SIZE, snd_msg.serial_num, 0);
		printf("Message Code Received: %d\n", rcv_msg.info.banker_message_code);

		// Inspect response type returned by Banker
        switch (rcv_msg.info.banker_message_code)
        {
        	case 4 :
        		printf("Banker's response: request granted\n");
        		printf("Sleeping......");
        		usleep(randr(100000, 500000));
        		break;
        	case 5 :
        		printf("Banker's response: request denied due to safety (Deadlock Avoidance algorithm)\n");

        		// Wait for random 100's of milliseconds then retry
        		int bytes = 0;
        		while (bytes == 0)
        		{
        			usleep(randr(100000, 500000));
        			sendMessage(&snd_msg, queID);
        			bytes = msgrcv(queID, &rcv_msg, MSG_INFO_SIZE, snd_msg.serial_num, 0);
        			if (bytes > 0)
        			{
        				for (int j = 0; j < NUM_RESOURCE_TYPES; j++)
        				{
        					params.allocated[j] += rcv_msg.info.request_vector[j];
        				}
        			// "Use" resources (.1:5 secs)
        				usleep(randr(100000, 500000));
        			}
        		}
        		break;
        	case 6 :
        		printf("Banker's response: request denied- Max claims would be exceeded");
        		// Adjust request vector and retry 5 times
        		for (int j = 0; j < 5; j++)
        		{
        			// Decrement request vector
        			for (int k = 0; k < NUM_RESOURCE_TYPES; k++)
        			{
        				if (snd_msg.info.request_vector[k] > 0)
        				{
        					snd_msg.info.request_vector[k]--;
        				}
        			}
        			// Resend message
        			sendMessage(&snd_msg, queID);
        			// Wait for reply
        			req_bytes = msgrcv(queID, &rcv_msg, MSG_INFO_SIZE, 0, 0);
        			//  Increment allocated[] and break if reply is granted
        			if (req_bytes > 0)
        			{
        				for (int j = 0; j < NUM_RESOURCE_TYPES; j++)
        				{
        					params.allocated[j] += rcv_msg.info.request_vector[j];
        				}
        				// "Use" resources (.1:5 secs)
        				usleep(randr(100000, 500000));

        				// Generate random release vector
        				for (int j = 0; j < NUM_RESOURCE_TYPES; j++)
        				{
        					snd_msg.info.request_vector[j] = randr(0, params.max_claim[j]);
        				}
        				// Compose and send the release message
        				message_code code = 2;
        				snd_msg.info.client_message_code= code;
        				serial_num++;
        				snd_msg.serial_num = serial_num;
        				snd_msg.info.client_pid = getpid();
        				snd_msg.info.client_queue = params.queue_id;
        				bytes = msgsnd(queID, &snd_msg, MSG_INFO_SIZE, 0);

        				// Wait for matching response from the banker
        				rls_bytes = msgrcv(queID, &rcv_msg, MSG_INFO_SIZE, snd_msg.serial_num, 1);
        				rcv_bool = 0;
        				break;
        			}
        		}
        		break;
        	case 7 :
        		printf("Banker's response: resources released successfully\n");
        		// Decrement allocated[]
        		for (int j = 0; j < NUM_RESOURCE_TYPES; j++)
        		{
        			params.allocated[j] -= rcv_msg.info.request_vector[j];
        		}
        		printf("Client's allocated counter has been updated to match the Banker.\n");
        		break;
        	case 8 :
        		printf("Banker's response: resources release failed: attempt to release more than allocated\n");
        		break;
        	case 9 :
        		printf("Banker's response: initial registration and max claims accepted\n");
        		break;
        	case 10 :
        		printf("Banker's response: initial registration and max claims rejected\n");

        		// Decrement by 1 until banker accepts max demands
        		if (snd_msg.info.banker_message_code == 10)
        		{
        			for (int i = 0; i < NUM_RESOURCE_TYPES; i++)
        			{
        				if (snd_msg.info.request_vector[i] > 0)
        					snd_msg.info.request_vector[i]--;
        			}
        		}

        		// Retry the request
        		snd_msg.info.request_vector[NUM_RESOURCE_TYPES] = params.max_claim[NUM_RESOURCE_TYPES];
        		bytes = msgsnd(queID, &snd_msg, MSG_INFO_SIZE, 0);
        		rcv_bool = 1;
        		break;
        	case 12 :
        		printf("Banker's response: request denied- Resources Currently Unavailable (may be safe)\n");
        		break;

        	// Waste random time (.1:5 secs) before next iteration
        }
        usleep(randr(100000, 5000000));
	}
	// Send final "release all resources" message
	printf("Here\n");
	msgBuf final_msg;
	message_code final = 2;
	serial_num++;
    final_msg.serial_num = serial_num;
	final_msg.info.client_pid = getpid();
	final_msg.info.client_id = params.id;
	final_msg.info.client_message_code = final;
	final_msg.info.request_vector[NUM_RESOURCE_TYPES] = params.allocated[NUM_RESOURCE_TYPES];
	bytes = msgsnd(queID, &snd_msg, MSG_INFO_SIZE, 0);

	// Free all memory and message queues
	/**
	free(params.resource_types);
	free(params.request_vector);
	free(params.allocated);
	free(params.max_claim);
	*/
	msgctl(queID,IPC_RMID,NULL);
	if (active_clients > 0)
		active_clients--;
    return 0;
}
#endif

